<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\User;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Doctrine\ORM\Query\Expr\Join;

class UserRepository extends EntityRepository implements UserLoaderInterface
{
	
	
	public function getPlatformRoles($userData)
	{
		
		$query = "
			select * from user_role where platform_table in (
				select group_concat(platform_id) from organization_platform where organization_id = :orgId group by platform_id
			)
		;
		";

		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'orgId', $userData['organization_id'], \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	public function login ($user) {
		
		
		$query = "
			SELECT 
				ur.user_role_id as role_id, 
				first_name, 
				last_name, 
				email, 
				u.user_id as id, 
				u.avatar_path, 
				u.organization_id,
				o.name as organization_name
			FROM
			    user u, user_role ur, organization o
			WHERE status = 'ACTIVO'
			AND u.user_id = :userId
			AND ur.user_role_id = u.user_role_id
			AND o.organization_id = u.organization_id
		;
		";

		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'userId', $user->getUserId(), \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
	/*public function loginClient($user)
	{
	    $query = "
			SELECT
		    u.client_id, u.default_client_group_id as default_group_id, u.name, u.tax_identifier, u.user, u.logo_path, sr.name as status, '3' as user_role_id
		FROM
		    rtp_client u
	    INNER JOIN status_repository sr on u.status_repository_id = sr.status_repository_id
		WHERE
			u.client_id = :userId
	    AND
	        u.status_repository_id = 1
	   
		;
		";
	    $res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
	    $res->bindValue ( 'userId', $user->getClientId(), \PDO::PARAM_STR );
	
	    $res->execute ();
	
	    return $res->fetch ();
	
	
	}*/
		

	public function getModuleCatalog($userRoleId)
	{
		$query = "
				SELECT 
					urmp.user_role_module_permission_id as urmp_id, 
					urmp.view_module as viewm,
					urmp.read_permission as rp, 
					urmp.write_permission as wp,
					urmp.edit_permission as ep, 
					urmp.delete_permission as dp,
					urmp.main_module as mm,
					mc.name as module_name,
					urmp.alias_name,
					mc.platform_id,
					m.name as platform_name,
					urmp.description as alias_description,
					mc.description as description, 
					mc.url_access as url_access,
					mc.module_catalog_id as mcid, 
					mc.module_icon,
					mc.visible,
					(SELECT COUNT(module_catalog_id) FROM module_catalog WHERE parent_id = mc.module_catalog_id) as count_children
				FROM
					user_role_module_permission urmp
				INNER JOIN
					module_catalog mc ON mc.module_catalog_id = urmp.module_id
				LEFT JOIN
					platform m ON mc.platform_id = m.platform_id	
				WHERE
					urmp.user_role_id = :userRoleId
				AND
					m.platform_id = mc.platform_id	
				
				ORDER BY
					mc.order_module ASC
		";
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( "userRoleId", $userRoleId, \PDO::PARAM_INT );
		//$res->bindValue ( "visible", 1, \PDO::PARAM_INT );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}

	/**
	 * Get children for module
	 *
	 * @param string $parentId id of parent module
	 * @return array() query result
	 */
	public function getModuleChildren($parentId, $userRoleId){
		$query = "
			SELECT 
				urmp.user_role_module_permissiom_id as urmp_id, urmp.view_module as viewm,
				urmp.read_permission as rp, urmp.write_permission as wp,
				urmp.edit_permission as ep, urmp.delete_permission as dp,
				urmp.main_module as mm, mc.name as module_name,
				mc.description as description, mc.url_access as url_access,
				mc.module_catalog_id as mcid, mc.module_icon
			FROM
				user_role_module_permission urmp
			INNER JOIN
				module_catalog mc ON mc.module_catalog_id = urmp.module_id
			WHERE
				urmp.user_role_id = :userRoleId
			AND
				mc.visible = :visible
			AND
				mc.parent_id = :parentId
			ORDER BY
					mc.order_module ASC
		";

		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( "userRoleId", $userRoleId, \PDO::PARAM_INT );
		$res->bindValue ( "visible", 1, \PDO::PARAM_INT );
		$res->bindValue ( "parentId", $parentId, \PDO::PARAM_INT );

		$res->execute ();

		return $res->fetchAll ();
	}

	public function loadUserByUsername($email) {
        return $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Gets Active list of users
     *
     * @return array() List of active users
     */
    public function getList($userData){
			
		$em = $this->getEntityManager();
			
		$query = "
			SELECT 
				ur.name as role_name,
				u.user_id as userId,
				u.first_name as firstName,
				u.last_name as lastName,				
				u.avatar_path as avatarPath,
				u.*
			FROM
			    user u, user_role ur
			WHERE status = 'ACTIVO'	
			AND ur.user_role_id = u.user_role_id			
			AND u.organization_id = :organization_id
		;
		";

		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );		
		$res->bindValue ( 'organization_id', $userData['organization_id'], \PDO::PARAM_STR );
		$res->execute ();
		
		return $res->fetchAll ();

		
	}
	
	
	
	public function findOneByMd5Id($md5Id) {
		$query = "
			SELECT
		    u.user_id
		FROM
		    user u
		WHERE
		    md5(u.user_id) = :md5Id
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
	
	public function findAgendaByMd5Id($md5Id) {
		$query = "
			SELECT
		    agenda_id,institute_id
		FROM
		    agenda
		WHERE
		    md5(agenda_id) = :md5Id
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'md5Id', $md5Id, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetch ();
	}
	
	public function getGradeSectionByInstitute($instituteId) {
		$query = "
			SELECT d.name as grade_name,p.name as plan_name, w.name as working_name, s.*
		FROM
		    grade_section s, grade d, plan p, working_day w
		WHERE
		    d.institute_id = :id
		AND
			d.grade_id = s.grade_id
		AND
			p.plan_id = d.plan_id
		AND 
			w.working_day_id = d.working_day_id		
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'id', $instituteId, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	public function getGradesByInstitute($instituteId) {
		$query = "
			SELECT g.grade_id, g.name as grade_name, p.name as plan_name, w.name as working_day_name
		FROM
		    grade g, plan p, working_day w
		WHERE
		   institute_id = :id	
		AND
			p.plan_id = g.plan_id
		AND 
			w.working_day_id = g.working_day_id	   		
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'id', $instituteId, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	public function getGradeSectionByGrade($gradeId) {
		$query = "
			SELECT d.name as grade_name,p.name as plan_name, w.name as working_name, s.*
		FROM
		    grade_section s, grade d, plan p, working_day w
		WHERE
		    d.grade_id = :id
		AND
			d.grade_id = s.grade_id
		AND
			p.plan_id = d.plan_id
		AND 
			w.working_day_id = d.working_day_id		
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'id', $gradeId, \PDO::PARAM_STR );
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	public function getStudentsBySection($sectionId) {
			
		
		$query = "
			SELECT u.*
		FROM
		    user u, grade_section g
		WHERE
		    u.grade_section_id = g.grade_section_id
		AND 
			u.user_role_id = 5  
		AND 
			u.grade_section_id = :id	 
		AND
			u.status = 'ACTIVO'		
		;
		";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'id', $sectionId, \PDO::PARAM_STR );		
		
		$res->execute ();
		
		return $res->fetchAll ();
	}


	
	
	public function getStudentsByUser($userId) {
			
		
		$query = "SELECT student_id FROM user_student WHERE user_id = :id";
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'id', $userId, \PDO::PARAM_STR );		
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	
	public function getTeachersByInstitute($instituteId,$userId = 0) {
			
		
		$query = "SELECT * FROM user WHERE user_role_id = 3 AND institute_id = :instituteId ";
		if($userId > 0)
		{
			$query .= " AND user_id = :uId ";
		}
		
		
		$res = $this->getEntityManager ()->getConnection ()->prepare ( $query );
		$res->bindValue ( 'instituteId', $instituteId, \PDO::PARAM_STR );		
		
		if($userId > 0)
		{
			$res->bindValue ( 'uId', $userId, \PDO::PARAM_STR );					
		}
		
		$res->execute ();
		
		return $res->fetchAll ();
	}
	
	
}
