<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Repository\Kaans;
use AppBundle\Entity\Teacher;
use AppBundle\Form\TeacherType;
use AppBundle\Helper\UrlHelper;

class TeacherController extends Controller {
	
	private $moduleId = 19;
	private $moduleName = "Docentes";


	/**
	 * @Route("/backend/teacher", name="backend_teacher")
	 */
	public function indexAction(Request $request) {
		$this->get ( "session" )->set ( "module_id",$this->moduleId);
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
	
		$teacher = new Teacher();
		$form = $this->createForm (new TeacherType(), $teacher);
		$form->handleRequest ($request);
		$em = $this->getDoctrine()->getManager();
       
        // Validar formulario
		if ($form->isSubmitted ()) {
			if ($form->isValid ()) {

                var_dump ($form);die;
				
			}
		}
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		return $this->render ( '@App/Backend/Teacher/index.html.twig', array(
            "form"    => $form->createView(),
			"permits" => $mp,
			"list"    => 'hola'
        ));
	}
	
	


}
