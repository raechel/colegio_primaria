<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Repository\Kaans;
use AppBundle\Helper\UrlHelper;

/**
 * Board controller.
 */
class DashboardController extends Controller {
	
	private $moduleId = 1;
	private $moduleName = "Tablero de calculos";


	/**
	 * @Route("/backend/dashboard", name="backend_dashboard")
	 */
	public function indexAction(Request $request) {
		$this->get ( "session" )->set ( "module_id",$this->moduleId);
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
	
		// $values01 = "['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC']";
		// $values02 = "['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC']";
		// $values1  = "['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC']";
		// $values2  = "['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC']";
		// $values3  = "['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC']";
		// $values4  = "['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC']";
		// $values5  = "['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC']";
		// $keys01 = "[2000,4900,2600,800,1800,600,2200,0,0,0,0,0]";
		// $keys02 = "[2000,4900,2600,800,1800,600,2200,0,0,0,0,0]";
		// $keys1  = "[2000,4900,2600,800,1800,600,2200,0,0,0,0,0]";
		// $keys2  = "[2000,4900,2600,800,1800,600,2200,0,0,0,0,0]";
		// $keys3  = "[2000,4900,2600,800,1800,600,2200,0,0,0,0,0]";
		// $keys4  = "[2000,4900,2600,800,1800,600,2200,0,0,0,0,0]";
		// $keys5  = "[2000,4900,2600,800,1800,600,2200,0,0,0,0,0]";
		// $to = '';
		// $from = '';
		// $mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		return $this->render ( '@App/Backend/Dashboard/index.html.twig');
	}
	

	/**
	 * @Route("/backend/dashboard/teacher", name="backend_dashboard_teacher")
	 */
	public function indexTeacherAction(Request $request) {
		
		
		return $this->render ( '@App/Backend/DashboardTeacher/index.html.twig');
	}

}
