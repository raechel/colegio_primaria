<?php

namespace AppBundle\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Repository\EbClosion;

class MainController extends Controller
{
	
    
    public function mainMenuAction(Request $request)
    {
    	$userData = $this->get("session")->get("userData");
    	$userModules = $this->get("session")->get("userModules");
    	$moduleId = $this->get("session")->get("module_id");
		
		$em = $this->getDoctrine()->getManager();
		$moduleGroups = $em->getRepository ( 'AppBundle:OrganizationPlatform' )->findBy(
			array("organization"=>$userData['organization_id'])
		);
		
		foreach($moduleGroups as $item)
		{
			$moduleGroup[$item->getPlatform()->getPlatformId()] = $item->getPlatform()->getName();			
		}
		
		
		
        // Get service with function
        $service = $this->container->get('user.role.handler');
        $roleId = $service->getRoleId();

        if ($roleId == 2) {
            $roleStaff = true;
        }
        else {
            $roleStaff = false;
        }
		

        return $this->render('@App/Backend/main_menu.html.twig', array(
        	"userData" => $userData,
        	"userModules" => $userModules,
        	"menuId" => $this->get("session")->get("module_id"),
        	"roleStaff" => $roleStaff,
        	"moduleGroup" => $moduleGroup
        ));
    }
    
    public function profileAction(Request $request)
    {
    	$userData = $this->get("session")->get("userData");
    	$userModules = $this->get("session")->get("userModules");
		$current = $this->get("session")->get("module_id");
		
		$moduleName = "";
		if($this->get("session")->get("module_name"))
		{
			if(strlen($this->get("session")->get("module_name"))>0)
			{
				$moduleName = $this->get("session")->get("module_name");
			}
		}
		
		
		switch($current)
		{
			case 1:
			case 10:
				$currentModule = "panel-header-lg";
			break;
			default:
				$currentModule = "panel-header-sm";
			break;	
		}

    	// replace this example code with whatever you need
    	return $this->render('@App/Backend/profile.html.twig', array(
    			"userData" => $userData,
    			"currentModule" => $currentModule,
    			"moduleName" => $moduleName,
    			"moduleId" => $current
    	));
    
    }
   
   
}
