<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Helper\UserHelper;
use AppBundle\Helper\UtilsHelper;
use AppBundle\Helper\UrlHelper;
use AppBundle\Helper\MailHelper;
use AppBundle\Repository\Kaans;
use Symfony\Component\HttpFoundation\JsonResponse;


class UserController extends Controller {
	
    
    private $moduleId = 2;
	private $moduleName = "Usuarios";
	
    
    /**
     * @Route("/backend/user", name="backend_user")
     */
	public function indexAction(Request $request) {
		
		$this->get("session")->set("module_id", $this->moduleId);	
		$this->get ( "session" )->set ( "module_name",$this->moduleName);	
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$user = new User ();
		$form = $this->createForm ( new UserType (), $user );
		$form->handleRequest ( $request );
		$em = $this->getDoctrine()->getManager();

		$thisUser = $em->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		// Validar formulario
		if ($form->isSubmitted ()) {
			if ($form->isValid ()) {
				// Encode password
				$error = $this->validateNewUser($user);

				if ($error) {
					// Add error flash
					$this->addFlash('error_message', $error);
				}
				else {
					//$user = $this->create($user, $this->getUser());					
					$em = $this->getDoctrine()->getManager();
					
					$file = $user->getAvatarPath();
					if($file)
					{						
						$fileName = md5(uniqid()) . '.' . $file->guessExtension();
						$file -> move($this -> getParameter('upload_files_profiles'), $fileName);						
						$user -> setAvatarPath($fileName);												
					} 
					
					$organization = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Organization' )->findOneBy(array('organizationId'=>$userData['organization_id']));
					$country = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Country' )->findOneBy(array('countryId'=>1));
						
				    // Encode password
				    $userHelper = $this->get('user.helper');
				    $user->setPassword($userHelper->encodePassword($user, $user->getPassword()));
					$user->setOrganization($organization);		
					$user->setCountry($country);										    					
				    $user->setCreatedBy($this->getUser());
				    $user->setCreatedAt(new \DateTime());			
				    $em->persist($user);
				    $em->flush();
					
					$this->addFlash ( 'success_message', $this->getParameter ( 'exito' ) );
					return $this->redirectToRoute ( "backend_user");
				}
			}
			else {
				$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ).$form->getErrorsAsString() );
			}
		}
		
		
		
		$queryUser = $em->getRepository ( 'AppBundle:User' )->getList ($userData);		
		$rolePlatforms = $em->getRepository ( 'AppBundle:User' )->getPlatformRoles($userData);
		
		
		//$pagination = $paginator->paginate ( $queryUser, $request->query->getInt ( 'page', 1 ), $this->getParameter ( "number_of_rows" ) );
		
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));		
		
		return $this->render ( '@App/Backend/User/index.html.twig', array (
				"form" => $form->createView (),
				"list" => $queryUser,
				"action" => "backend_user_index",
		        "permits" => $mp,
		        'userInfo'=>$thisUser,		        
		        'department' => $department,
		        'userData'=>$userData,
		        'rolePlatforms'=>$rolePlatforms		        
		) );
	}
	
	/**
	 * @Route("/backend/user/edit/{id}", name="backend_user_edit")
	 */
	public function editAction(Request $request) {
		
		$userData = $this->get ( "session" )->get ( "userData" );
		$md5 = $request->get ( "id" );
		
		$userId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneByMd5Id($md5);
		$user   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userId,'status'=>"ACTIVO"));
		$em = $this->getDoctrine()->getManager();
		
		$thisUser = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userData['id']));
		
		if ($user) {
		
			$oldPassword = $user->getPassword ();
			$oldEmail = $user->getEmail ();
			$oldImage = $user -> getAvatarPath();
		
			$form = $this->createForm ( new UserType (), $user );
			$form->handleRequest ( $request );
			if ($form->isSubmitted ()) {
				if ($form->isValid ()) {
					$error = $this->validateEditUser($user, $oldEmail);

					if ($error) {
						$this->addFlash('error_message', $error);
					}
					else {
						
						$file = $user->getAvatarPath();
						if ($file) {
							$fileName = md5(uniqid()) . '.' . $file->guessExtension();
							$file -> move($this -> getParameter('upload_files_profiles'), $fileName);
							
							
							$user -> setAvatarPath($preUrl.$fileName);
							
							$userData['avatar_path'] = $fileName;
							$userData['email'] = $user->getEmail();
							$userData = $this->get ( "session" )->set ( "userData",$userData );
							
							
						} else {
							$user -> setAvatarPath($oldImage);
						}
						
						$user = $this->edit($user, $oldPassword, $this->getUser());
					}

					$this->addFlash ( 'success_message', $this->getParameter ( 'exito_actualizar' ) );
				} else {
					// Error validation
					$this->addFlash ( 'error_message', $this->getParameter ( 'error_form' ) );
				}
			}

			$rolePlatforms = $em->getRepository ( 'AppBundle:User' )->getPlatformRoles($userData);
			$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
			
			return $this->render ( '@App/Backend/User/edit.html.twig', array(
				"form" => $form->createView (),
				"edit" => true,
				'userInfo'=>$thisUser,
				'user'=>$user,					
				"permits" => $mp,
				'rolePlatforms'=>$rolePlatforms					
			) );
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_editar' ) );
		}
		return $this->redirectToRoute ( "backend_user");
	}
	
	
	
	 public function returnPDFResponseFromHTML($html,$user,$cycle_number){
        //set_time_limit(30); uncomment this line according to your needs
        // If you are not in a controller, retrieve of some way the service container and then retrieve it
        //$pdf = $this->container->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //if you are in a controlller use :
        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Kaans - Plataforma Educativa');
		
		$subject = 'reporte_académico_'.$user->getFirstName()."_".$user->getLastName()."_".$cycle_number;
        $pdf->SetTitle($subject);
        $pdf->SetSubject($subject);
		$pdf->SetHeaderData("", "", "", "Reporte Académico de ".$user->getFirstName()." ".$user->getLastName());
				
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 10, '', true);
        $pdf->SetMargins(15,20,15, true);
        $pdf->AddPage();
        
        $filename = $subject;
        
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
    }


	/**
	 * @Route("/backend/user/view/{id}", name="backend_user_view")
	 */
	public function viewAction(Request $request) {
		
		$md5 = $request->get ( "id" );
		$userData = $this->get ( "session" )->get ( "userData" );
		
		$userId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneByMd5Id($md5);
		$user   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneByUserId($userId);
		
		
		if ($user) {
		
		
			
		} else {
			return $this->redirectToRoute ( "backend_user" );
		}

		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		$dashboard      = array();
		$dashboard_root = array();
		$cycle_number   = 0;
		$selected_cycle_full_name = "";
		$assignedSubjects = array();
		$activitiesTeacher = array();
					
		if($user->getUserRole()->getUserRoleId() == 5 || $user->getUserRole()->getUserRoleId() == 6)
		{
				
			$api_url = $this->getParameter('api_url');
			$url = $api_url.'backend_dashboard.php?id='. $this->base64_url_encode($user->getUserId());	
			
			$urlHelper = new UrlHelper(); 
			
			$result = json_decode($urlHelper->open_url($url));
			if($result)
			{
				$dashboard_root = $result;
				$cycle_number = $dashboard_root->current_cycle_number;
				if($request->get('filter_cycle'))
				{			
					$cycle_number = $request->get('filter_cycle');
				}
				
				if(strlen($cycle_number)==0)
				{
					$cycle_number = 1;
				}		
				
				$dashboard = $dashboard_root->dashboard->$cycle_number;
			}
			

			foreach($dashboard_root->cycle_array as $value)
			{
				if($cycle_number == $value->cycle_number)
				{
					$selected_cycle_full_name = $value->cycle_full_name;
				}
			}
			
		}	
		
		if($user->getUserRole()->getUserRoleId() == 3)
		{
			$assignedSubjects = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:SubjectGradeSection' )->findBy(array('assignedUser'=>$user));
			$activitiesTeacher = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:Task' )->findBy(array('createdBy'=>$user));
		}			
		
		$parameters = array (				
				"user" => $user,				
		        "permits" => $mp,
		        'dashboard' => $dashboard,
		        'dashboard_root'=>$dashboard_root,
		        'selected_cycle'=>$cycle_number,
		        'selected_cycle_full_name' => str_replace(" (Actual)","",$selected_cycle_full_name),
		        'userData'=>$userData,
		        'md5Id'=>$md5,
		        
		        'assigned_subjects'=>$assignedSubjects,
		        'activities_teacher'=>$activitiesTeacher
			);
			
		
		if($request->get('print'))
		{	
			
			if($request->get('total_only'))
			{
				$html =  $this->renderView ( '@App/Backend/User/print_total_only.html.twig', $parameters);			
			}	else {	
				$html =  $this->renderView ( '@App/Backend/User/print.html.twig', $parameters);
			}
			$this->returnPDFResponseFromHTML($html,$user,$cycle_number);
			
		} else {
			
			return $this->render ( '@App/Backend/User/view.html.twig', $parameters);
		}
		
		
		
	}



	
	/**
	 * @Route("/backend/user/delete/{id}", name="backend_user_delete")
	 */
	public function deleteAction(Request $request) {
			
		$md5 = $request->get ( "id" );
		$userId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneByMd5Id($md5);
		$user   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneByUserId($userId);
		
		if ($user) {
			// Can't delete yourself
			if ($user->getUserId() != $this->getUser()->getUserId()) {
				$em = $this->getDoctrine ()->getManager ();

				// Make inactive
				$user->setStatus('INACTIVO');
				$em->persist( $user );
				$em->flush ();
				
				$this->addFlash ( 'success_message', $this->getParameter ( 'exito_eliminar' ) );
			}
			else {
				$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ) );
			}
		} else {
			$this->addFlash ( 'error_message', $this->getParameter ( 'error_eliminar' ) );
		}
		
		return $this->redirectToRoute ( "backend_user", array('did'=>$did) );
	}

	/**
	 * Encodes password, sets created at and by and stores in db
	 * 
	 * @param User $user the entity to be created
	 * @param User $creator the user who creates a user
	 * @return User user saved in db
	 */
	public function create($user, $creator) {
		

	    //return $user;
	}

	/**
	 * Validates the information of a new user
	 *
	 * @param User $user possible new user
	 * @return string error message. NULL if no error
	 */
	public function validateNewUser($user) {
		$em = $this->getDoctrine()->getManager();

	    // Check for duplicate email
	    $original = $em->getRepository('AppBundle:User')->findOneByEmail($user->getEmail());

	    if ($original) {
	        // Email already in database
	        return $this->getParameter('usuario_existente');
	    }

	    return null;
	}

	public function edit($user, $oldPassword, $editor){
		$em = $this->getDoctrine()->getManager();

		// If no password provided, use last password
		if ($user->getPassword() == "") {
			$user->setPassword($oldPassword);
		}
		else {
			// Encode password
			$userHelper = $this->get('user.helper');
			$user->setPassword($userHelper->encodePassword($user, $user->getPassword()));
		}

		// Set updated By
		$user->setUpdatedBy($editor);
		$user->setUpdatedAt(new \DateTime());

		

		// Store in DB
		$em->persist($user);
		$em->flush();

		return $user;
	}
	
	/**	 
	 * @Route("/backend/dynamic/grade/section/{id}", name="backend_dynamic_grade_section", defaults={"id":"0"})
	 */
	 public function dynamicGradeSection($id)
	 {
		
		$sections = $this->getDoctrine ()->getRepository ( 'AppBundle:User' )->getGradeSectionByInstitute($id);			
			
		return $this->render ( '@App/Backend/User/sections.html.twig', array (
			"sections" => $sections 
		));
		
	 }

	/**	 
	 * @Route("/backend/dynamic/users/section/{sectionId}", name="backend_dynamic_users_section", defaults={"sectionId":"0"})
	 */
	 public function dynamicUsersSectionByGrade($sectionId)
	 {
		
		//Selecciona maestros, alumnos
		$users = $this->getDoctrine ()->getRepository ( 'AppBundle:User' )->getStudentsBySection($sectionId);			
			
		return $this->render ( '@App/Backend/User/users.html.twig', array (
			"users" => $users 
		));
		
	 }

	/**	 
	 * @Route("/backend/dynamic/bygrade/section/{gradeId}", name="backend_dynamic_bygrade_section", defaults={"gradeId":"0"})
	 */
	 public function dynamicGradeSectionByGrade($gradeId)
	 {
		
		$sections = $this->getDoctrine ()->getRepository ( 'AppBundle:User' )->getGradeSectionByGrade($gradeId);			
			
		return $this->render ( '@App/Backend/User/sections.html.twig', array (
			"sections" => $sections 
		));
		
	 }
	 
	 
    /**	 
	 * @Route("/backend/dynamic/grade/institute/{instituteId}", name="backend_dynamic_grade_institute", defaults={"instituteId":"0"})
	 */
	 public function dynamicGradeByInstitute($instituteId)
	 {
		
		$grades = $this->getDoctrine ()->getRepository ( 'AppBundle:User' )->getGradesByInstitute($instituteId);			
			
		return $this->render ( '@App/Backend/User/grades.html.twig', array (
			"grades" => $grades 
		));
		
	 }
	 

	/**	 
	 * @Route("/backend/user/recovery", name="backend_user_password_recovery")
	 */
	 public function passwordRecovery(Request $request)
	 {
	 		
		return $this->render ( '@App/Backend/login.html.twig', array (
			
		));
		
	 }	
	 
	/**
	 * @Route("/backend/user/password/change", name="backend_user_password_change")
	 */
	 public function passwordChange(Request $request)
	 {
	 	$md5 = $request->get('id');
		
		$values = $request->get('pass');
		$oldPass = $values['old'];
		$pass_raw = $values['new'];
		
		$userId = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneByMd5Id($md5);
		$usercheck   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userId));
				
		$userHelper = $this->get('user.helper');
		$oldPassword = $userHelper->encodePassword($usercheck, $oldPass);
		$pass = $userHelper->encodePassword($usercheck, $pass_raw);
			
		
		$user   = $this->getDoctrine()->getManager()->getRepository ( 'AppBundle:User' )->findOneBy(array('userId'=>$userId,'password'=>$oldPassword));
		
		if($user)
		{
			
			$em = $this->getDoctrine()->getManager();				
			$user->setPassword($pass);
			$em->persist($user);
			$em->flush();
			$this->addFlash ( 'success_message', "La contraseña ha sido cambiada correctamente." );	
		} else {
			$this->addFlash ( 'error_message', "La contraseña actual no es correcta." );
		}
		

				
		return $this->redirectToRoute ( "backend_user_edit" ,array("id"=>$md5));	
	 }	
	 
	/**	 
	 * @Route("/backend/user/recovery/app", name="backend_user_password_recovery_app")
	 */
	 public function passwordRecoveryAppCheck(Request $request)
	 {

	 	$response = new JsonResponse();
		$data = $this->get("request")->getContent();
		if(!empty($data)) {
		    $params = json_decode($data, true);
		}		
		
		$email = $params['_email'];
		$user = $this->getDoctrine ()->getRepository ( 'AppBundle:User' )->findOneBy(array('email'=>$email));
		if($user)
		{
				
			$utils = new UtilsHelper();
			$userHelper = $this->get('user.helper');		
						
			$pass = $utils->randomChars(6);			
			$user->setPassword($userHelper->encodePassword($user, $pass));
			
			$em = $this->getDoctrine()->getManager();								    
		    $em->persist($user);
		    $em->flush();
			
			$mailHelper = new MailHelper();
			$mailHelper->recoverPassword($user,$pass);
			
			$response->setData(array('status' => 'success'));
				
		} else {		
			$response->setData(array('status' => 'error'));
		}
		
	 	return $response;
		
	 }
	 
	/**	 
	 * @Route("/backend/user/recovery/check", name="backend_user_password_recovery_check")
	 */
	 public function passwordRecoveryCheck(Request $request)
	 {
	 	
		
		$email = $request->get("_email");
		$user = $this->getDoctrine ()->getRepository ( 'AppBundle:User' )->findOneBy(array('email'=>$email));
		if($user)
		{
				
			$utils = new UtilsHelper();
			$userHelper = $this->get('user.helper');		
						
			$pass = $utils->randomChars(6);			
			$user->setPassword($userHelper->encodePassword($user, $pass));
			
			$em = $this->getDoctrine()->getManager();								    
		    $em->persist($user);
		    $em->flush();
			
			$mailHelper = new MailHelper();
			$mailHelper->recoverPassword($user,$pass);
			
			$this->addFlash('success_message', "Hemos enviado un email con instrucciones a $email,<br>por favor verifique su bandeja de entrada.");
				
		} else {		
			$this->addFlash('error_message', "Email incorrecto, intente de nuevo.");
		}
		
	 	return $this->redirectToRoute ( "backend_user_password_recovery" );	
	 }

	/**
	 * Validates the information of a editing user
	 *
	 * @param User $user user to edit
	 * @param string $oldEmail the email before editing
	 * @return string error message. NULL if no error
	 */
	public function validateEditUser($user, $oldEmail) {
		$em = $this->getDoctrine()->getManager();

	    // Check for duplicate email
	    $original = null;
	    if ($user->getEmail() != $oldEmail) {
	    	// Look if duplicate
		    $original = $em->getRepository('AppBundle:User')->findOneByEmail($user->getEmail());
	    }

	    if ($original) {
	        // Email already in database
	        return $this->getParameter('usuario_existente');
	    }

	    return null;
	}
	
	public function base64_url_encode($input) {
	 return strtr(base64_encode($input), '+/=', '._-');
	}
}
