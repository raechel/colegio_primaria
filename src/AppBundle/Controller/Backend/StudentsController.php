<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Repository\Kaans;
use AppBundle\Entity\Students;
use AppBundle\Form\StudentsType;
use AppBundle\Helper\UrlHelper;

class StudentsController extends Controller {
	
	private $moduleId = 17;
	private $moduleName = "Estudiantes";


	/**
	 * @Route("/backend/students", name="backend_students")
	 */
	public function indexAction(Request $request) {
		$this->get ( "session" )->set ( "module_id",$this->moduleId);
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
	
		$students = new Students();
		$form = $this->createForm (new StudentsType(), $students);
		$form->handleRequest ($request);
		$em = $this->getDoctrine()->getManager();
       
        // Validar formulario
		if ($form->isSubmitted ()) {
			if ($form->isValid ()) {

                var_dump ($form);die;
				
			}
		}
		$mp = Kaans::getModulePermission($this->moduleId, $this->get("session")->get("userModules"));
		
		return $this->render ( '@App/Backend/Students/index.html.twig', array(
            "form"    => $form->createView(),
			"permits" => $mp,
			"list"    => 'hola'
        ));
	}
	
	


}
