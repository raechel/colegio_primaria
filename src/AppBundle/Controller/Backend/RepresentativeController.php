<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Repository\Kaans;

use AppBundle\Entity\Representative;
use AppBundle\Form\RepresentativeType;
use AppBundle\Helper\UrlHelper;

class RepresentativeController extends Controller {
	
	
	private $moduleName = "Representantes";


	/**
	 * @Route("/backend/representative", name="backend_representative")
	 */
	public function indexAction(Request $request) {
		$this->get ( "session" )->set ( "module_name",$this->moduleName);
	
		$representative = new Representative();
		$form = $this->createForm (new RepresentativeType(), $representative);
		$form->handleRequest ($request);
		$em = $this->getDoctrine()->getManager();
       
        // Validar formulario
		if ($form->isSubmitted ()) {
			if ($form->isValid ()) {

                var_dump ($form);die;
				
			}
		}
		
		return $this->render ( '@App/Backend/Representative/index.html.twig', array(
            "form"    => $form->createView(),
			"list"    => 'hola'
        ));
	}
	
	


}
