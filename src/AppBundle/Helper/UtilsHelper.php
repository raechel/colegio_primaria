<?php
namespace AppBundle\Helper;

class UtilsHelper
{
	
	
	
	public function randomChars($random_string_length)
	{
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
		
		$string = '';
		$max = strlen($characters) - 1;
		for ($i = 0; $i < $random_string_length; $i++) {
		    $string .= $characters[mt_rand(0, $max)];
		}
		
		return $string; 
	}
	
	
	
	
	public function featureList($noEmpty = false)
	{
		$arr = array();
		$arr[] = array("for"=>"Directores","icon"=>'line-chart',"title"=>"Estadísticas Importantes en tiempo real","description"=>"Podrá ver Reportes y Estadisticas del rendimiento de todo su Centro Educativo y de sus docentes");
		$arr[] = array("for"=>"Directores","icon"=>'users',"title"=>"Estado de todos los Alumnos","description"=>"Visualizar, en tiempo real, las tareas y notas de todos los alumnos, en todos los ciclos");
		$arr[] = array("for"=>"Directores","icon"=>'comment',"title"=>"Circulares ilimitadas para todos","description"=>"Enviar circulares ilimitadas al celular de alumnos y padres de uno o todos los grados");
		$arr[] = array("for"=>"Directores","icon"=>'calendar',"title"=>"Agenda actualizable 100% dígital","description"=>"Administrar una agenda virtual con las actividades de todo el año, para uno o todos los grados ¡Adios al calendario impreso!");		
		if(!$noEmpty){ $arr[] = array("for"=>"Directores","icon"=>'',"title"=>"","description"=>""); }				
		$arr[] = array("for"=>"Directores","icon"=>'files-o',"title"=>"Cuadros finales sin dolores de cabeza","description"=>"Generar cuadros finales para reportar al MINEDUC. ¡Adios a los dolores de cabeza de fin de año! ");
		$arr[] = array("for"=>"Directores","icon"=>'money',"title"=>"Administración financiera fácil y rápida","description"=>"Podrá gestionar las cuentas por pagar y los abonos de cada padre de familia de forma fácil y rapida. Así como desactivar usuarios por morosidad.");
		if(!$noEmpty){ $arr[] = array("for"=>"Directores","icon"=>'',"title"=>"","description"=>""); }
		
		$arr[] = array("for"=>"Docentes","icon"=>'tasks',"title"=>"Crear tareas para cada grado y sección","description"=>"Crear tareas fácil y rápido, para que sean entregadas físicamente o en línea. Todas las tareas podran ser vistas por Alumnos y Padres en tiempo real.");
		$arr[] = array("for"=>"Docentes","icon"=>'check',"title"=>"Calificar todas las tareas una sola vez","description"=>"Alumnos, Padres y Dirección podran ver la calificación inmediatamente. ¡Adios a calificar hasta 4 veces la misma tarea!");
		$arr[] = array("for"=>"Docentes","icon"=>'comment',"title"=>"Circulares ilimitadas para todos","description"=>"Enviar circulares ilimitadas al celular de alumnos y padres de uno o todos los grados");
		$arr[] = array("for"=>"Docentes","icon"=>'calendar',"title"=>"Agenda actualizable 100% dígital","description"=>"Administrar una agenda virtual con las actividades de todo el año, para uno o todos los grados ¡Adiós al calendario impreso!");		
		if(!$noEmpty){ $arr[] = array("for"=>"Docentes","icon"=>'',"title"=>"","description"=>""); }		
		$arr[] = array("for"=>"Docentes","icon"=>'list-ul',"title"=>"Consulta Histórica en todos los ciclos","description"=>"Visualizar el histórico de tareas y notas de sus alumnos, en todos los ciclos");
		$arr[] = array("for"=>"Docentes","icon"=>'play',"title"=>"Recursos de apoyo para clases","description"=>"Videos multi tema para usarlos como soporte en la impartición y preparación de clases y diccionarios de los principales Idiomas Mayas de Guatemala.");
		if(!$noEmpty){ $arr[] = array("for"=>"Docentess","icon"=>'',"title"=>"","description"=>""); }			

		$arr[] = array("for"=>"Alumnos","icon"=>'tasks',"title"=>"Información de tareas por materia","description"=>"Ver todas las tareas por materia, separadas por tareas entregadas, pendientes y por ciclo");
		$arr[] = array("for"=>"Alumnos","icon"=>'calendar',"title"=>"Agenda digital actualizada en tiempo real","description"=>"Ver la agenda y calendario virtual del año, con todas las actividades de sus grados.");
		$arr[] = array("for"=>"Alumnos","icon"=>'upload',"title"=>"Entregar tareas digitalmente","description"=>"Entregar tareas digitales, fácilmente, desde la plataforma web");
		$arr[] = array("for"=>"Alumnos","icon"=>'star',"title"=>"Visualizar Estado Académico","description"=>"Visualizar su estado académico completo, por materia, de forma rápida y fácil de entender");
		if(!$noEmpty){ $arr[] = array("for"=>"Alumnos","icon"=>'',"title"=>"","description"=>""); }	
		$arr[] = array("for"=>"Alumnos","icon"=>'play',"title"=>"Enciclopedia en video multi tema","description"=>"Separadas por materia, para utilizarlos como apoyo adicional a las clases recibidas");
		$arr[] = array("for"=>"Alumnos","icon"=>'book',"title"=>"Diccionario de Idiomas Mayas de Guatemala","description"=>"Un recurso exclusivo de Kaans. El diccionario esta ordenado por lengua y contiene un buscador facil de usar.");
		if(!$noEmpty){ $arr[] = array("for"=>"Alumnos","icon"=>'',"title"=>"","description"=>""); }			

		$arr[] = array("for"=>"Padres","icon"=>'tasks',"title"=>"Enterarse de todas las tareas","description"=>"De todos sus hijos, aunque estén en grados diferentes. Desde la comodidad de su celular o computadora. ¡Adios a las carreras del domingo por la noche");
		$arr[] = array("for"=>"Padres","icon"=>'calendar',"title"=>"Agenda digital actualizada en tiempo real","description"=>"Ver la agenda y calendario virtual del año, con todas las actividades de los grados de sus hijos. Desde su celular o computadora.");
		$arr[] = array("for"=>"Padres","icon"=>'comment',"title"=>"Recibir Circulares Digitales","description"=>"Recibir circulares enviadas por directores o maestros, con temas relacionadas a los grados de sus hijos");
		$arr[] = array("for"=>"Padres","icon"=>'line-chart',"title"=>"Ver el estado académico de sus hijos","description"=>"Ver como van sus hijos, en todas las materias, en cuestión de minutos. ¡Sin llamar o visitar el Centro Educativo!");
		
		return $arr;		
			 
	}
	
	
	
	public function getCycleName($cycle)
	{
		switch($cycle)
		{
			case 1:
				$name = 'Mes';
			break;
			case 2:
				$name = 'Bimestre';
			break;		
			case 3:
				$name = 'Trimestre';
			break;	
			case 4:
				$name = 'Cuatrimestre';
			break;	
			case 5:
				$name = 'Quintumestre';
			break;	
			case 6:
				$name = 'Semestre';
			break;					
		}
		
		return $name;
	}
	
	public function getMonthsArray($searchNumber = false)
	{
		$months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		
		if($searchNumber)
		{
			
			return $months[$searchNumber-1];
		}		
		
		return $months;
	}
	
	public function orderCycles($cycle = 2, $isNumberOfMonth = false)
	{
			
		$months = $this->getMonthsArray();
		
		$monthsCount = count($months) - 1;
		$u = 0;		
		$thisCycle = 1;
		
		$arr = array();
		for($i = 0;$i<=$monthsCount;$i++)
		{
			if($months[$i])
			{
				$arr[$thisCycle][] = $months[$i];
				$u++;
				if($u == $cycle)
				{
					$u = 0;
					$thisCycle++;
				}
			}
	
		}
		
		
		return $arr;
	}
	
	public function cycleInMonth($monthName,$cycles)
	{
		
		$arr = $this->orderCycles($cycles);
		foreach($arr as $key => $cycle)
		{
			
			if(in_array($monthName,$cycle))
			{
				return $key;	
				//print_r($cycle);			
				exit;
			};
		}
	}
	
	public function ordinal($number) {
	    $ends = array('','er','do','er','to','to','to','mo','vo','no','mo');
	    if ((($number % 100) >= 11) && (($number%100) <= 13))
	        return $number. 'er';
	    else
	        return $number. $ends[$number % 10];
	}
	
	public function cycleInfo($insInfo)
	{
		$months = $this->getMonthsArray();
		$totalMonths = count($months);
		
		$cycleArray = array();
		$resultArr = array();
		$u = 0;
		
		for($i = 0; $i<=$totalMonths; $i++)
		{
			
			$cycleNumber = $this->cycleInMonth($months[$i],$insInfo->getCycle());
			$monthNumberRaw = $i+1;
			$monthNumber = (strlen($monthNumberRaw) == 1 ? '0'.$monthNumberRaw : $monthNumberRaw);
			
			$dateFrom = date('Y')."-".$monthNumber.'-01';
			$dateTo = date('Y')."-".$monthNumber.'-31';
			
			$cycleName = $this->getCycleName($insInfo->getCycle());
			
			$monthName = $this->getMonthsArray($monthNumber);			
			$cycleNumberOrdinal = $this->ordinal($cycleNumber);			
			$cycleFullName = $cycleNumberOrdinal." ".$cycleName;
			
			
			//Esto sirve para obtener el rango de meses del bimestre
			$u++;
			$tempArray[] = array(
				'date_from'=>$dateFrom,
				'date_to'=>$dateTo	
			);
			
			//Cada ciclo (2,4, etc según el instituto) realizamos la revisión de las tareas dentro del rango de fecha
			if($u == $insInfo->getCycle())
			{
				
				$cycleArray[] = array('cycle_full_name'=>$cycleFullName,'cycle_number'=>$cycleNumber);
				
				//Con esto reseteamos el contador del ciclo para obtener el rango de meses		
				$tempArray = array();
				$u = 0;
			};
			
						
		};
		
		
		
		$currentMonthName = $this->getMonthsArray(date('m'));	
		$currentCycle = $this->cycleInMonth($currentMonthName,$insInfo->getCycle());		
			 	
		//Limpiamos el array de registros del futuro, que no tienen contenido
		//Aca se pueden tambien hacer modificaciones de los nombres de los ciclos
		$totalCycleArray = count($cycleArray);
		for($o=0;$o<=$totalCycleArray;$o++)
		{
			//Borramos registros futuros del ciclo
			if($cycleArray[$o]['cycle_number'] > $currentCycle)
			{
				unset($cycleArray[$o]);
			}
			
			//Modificamos el nombre del ciclo actual
			if($cycleArray[$o]['cycle_number'] == $currentCycle)
			{
				$cycleArray[$o]['cycle_full_name'] = $cycleArray[$o]['cycle_full_name']." (actual)";
			}
		}		
				
		$response = array(						
			'current_cycle_number'=>$currentCycle,
			'cycle_array'=>$cycleArray
		);
		
		
		return $response;
		
	}	
}		
	