<?php
namespace AppBundle\Helper;

include 'Mailin.php';
include 'Template.php';

class MailHelper
{
	



	
	public function recoverPassword($userObject,$pass)
	{
		

		$templateClass = new Template();
		$template = $templateClass->getMain();
		
		$subject = 'Recuperación de contraseña';
		$title = '¡Hola '.$userObject->getFirstName().'!';
		$content = '<p>Al parecer has olvidado tu contraseña. Utiliza la siguiente contraseña temporal, para ingresar a la plataforma y establecer una nueva:</p><h2>'.$pass.'</h2><p>Tu usuario es: <b>'.$userObject->getUsername().'</b> </p>';
		
		$html_replaced = str_replace("_TITLE_",$title,$template);
		$html_replaced = str_replace("_CONTENT_",$content,$html_replaced);
		
		$this->sendEmail($html_replaced,$subject,$userObject->getEmail());
		
	}
	
	public function publicContactForm($email,$name,$phone,$msg,$type = 2)
	{
		$templateClass = new Template();
		$template = $templateClass->getMain();
		
		if($type == 1)
		{
			$subject = 'Solicitud de visita de '.$name;
			$msg = 'Solicitud de visita desde sitio web';
		} else {
			$subject = 'Mensaje de '.$name;
		}
		$content = 'El siguiente mensaje fue enviado desde el formulario de contácto publico<br><br><b>Nombre:</b> '.$name.'<br><b>Email:</b> '.$email.'<br><b>Teléfono: </b>'.$phone.'<br><b>Mensaje:</b> '.$msg;
		$title = '<b>Nuevo mensaje</b>';
		
		$html_replaced = str_replace("_TITLE_",$title,$template);
		$html_replaced = str_replace("_CONTENT_",$content,$html_replaced);
		
		$res = $this->sendEmail($html_replaced,$subject,'info@kaans.net');
		return $res;		
	}
	
	public function sendEmail($template,$subject,$toEmail)
	{
	
		
		$mailin = new Mailin('servicio@aguilalibreweb.com', 'nNzYrj87cpfKMP4k');
		$mailin->
		addTo($toEmail, $toEmail)->
		setFrom('notificaciones@kaans.net', 'Kaans')->
		setReplyTo('soporte@kaans.com','Soporte Kaans')->
		setSubject($subject)->
		setCc('servicio@aguilalibreweb.com')->
		//setText('Hello')->
		setHtml($template);
		$res = $mailin->send();	
		//print_r($res);exit;
		return $res;
		
	}
	
}		
	