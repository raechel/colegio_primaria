<?php
namespace AppBundle\Helper;

class UrlHelper
{
    /**
     * Shortens a url using google api
     *
     * @param string $longUrl url to shorten
     * @return string short (tiny) url. NULL if it wasn't possible to shorten url
     */
    public function tinyUrl($longUrl){
        // Get API key from : http://code.google.com/apis/console/
        $apiKey = 'AIzaSyASQnLV23ZycSb1BTY0LZ128gYuvAdnCTw';

        $postData = array('longUrl' => $longUrl, 'key' => $apiKey);
        $jsonData = json_encode($postData);

        $curlObj = curl_init();

        curl_setopt($curlObj, CURLOPT_URL,
            'https://www.googleapis.com/urlshortener/v1/url?key='.$apiKey);
        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlObj, CURLOPT_HEADER, 0);
        curl_setopt($curlObj, CURLOPT_HTTPHEADER,
            array('Content-type:application/json'));
        curl_setopt($curlObj, CURLOPT_POST, 1);
        curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

        $response = curl_exec($curlObj);

        // Change the response json string to object
        $json = json_decode($response);

        curl_close($curlObj);

        if(property_exists($json, 'id')){
            return $json->id;
        }

        return NULL;
    }

    
    public function open_url($url = "", $parameters = false) {
    	$ch = curl_init ();
    	curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
    	curl_setopt ( $ch, CURLOPT_SSL_VERIFYHOST, false );
    	curl_setopt ( $ch, CURLOPT_URL, $url );
    	curl_setopt ( $ch, CURLOPT_HEADER, 0 );
    	if ($parameters) {
    		curl_setopt ( $ch, CURLOPT_POST, true);
    		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $parameters);
    	}
    
    	ob_start ();
    	curl_exec ( $ch );
    	curl_close ( $ch );
    	$string = ob_get_contents ();
    	ob_end_clean ();
    	return $string;
    }
    
    
}
?>