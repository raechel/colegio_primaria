<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrganizationPlatform
 */
class OrganizationPlatform
{
    /**
     * @var integer
     */
    private $organizationPlatformId;

    /**
     * @var integer
     */
    private $organizationId;

    /**
     * @var integer
     */
    private $platformId;


    /**
     * Get organizationPlatformId
     *
     * @return integer 
     */
    public function getOrganizationPlatformId()
    {
        return $this->organizationPlatformId;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     * @return OrganizationPlatform
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer 
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set platformId
     *
     * @param integer $platformId
     * @return OrganizationPlatform
     */
    public function setPlatformId($platformId)
    {
        $this->platformId = $platformId;
    
        return $this;
    }

    /**
     * Get platformId
     *
     * @return integer 
     */
    public function getPlatformId()
    {
        return $this->platformId;
    }
    /**
     * @var \AppBundle\Entity\Platform
     */
    private $platform;

    /**
     * @var \AppBundle\Entity\Organization
     */
    private $organization;


    /**
     * Set platform
     *
     * @param \AppBundle\Entity\Platform $platform
     * @return OrganizationPlatform
     */
    public function setPlatform(\AppBundle\Entity\Platform $platform = null)
    {
        $this->platform = $platform;
    
        return $this;
    }

    /**
     * Get platform
     *
     * @return \AppBundle\Entity\Platform 
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * Set organization
     *
     * @param \AppBundle\Entity\Organization $organization
     * @return OrganizationPlatform
     */
    public function setOrganization(\AppBundle\Entity\Organization $organization = null)
    {
        $this->organization = $organization;
    
        return $this;
    }

    /**
     * Get organization
     *
     * @return \AppBundle\Entity\Organization 
     */
    public function getOrganization()
    {
        return $this->organization;
    }
}
