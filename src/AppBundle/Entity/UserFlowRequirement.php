<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserFlowRequirement
 */
class UserFlowRequirement
{
    /**
     * @var integer
     */
    private $userFlowRequirementId;

    /**
     * @var integer
     */
    private $stepRequirementId;

    /**
     * @var integer
     */
    private $organizationId;

    /**
     * @var integer
     */
    private $stepId;

    /**
     * @var integer
     */
    private $flowId;

    /**
     * @var integer
     */
    private $statusRepositoryId;

    /**
     * @var integer
     */
    private $accomplishedBy;

    /**
     * @var \DateTime
     */
    private $accomplisehdAt;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var integer
     */
    private $createdBy;


    /**
     * Get userFlowRequirementId
     *
     * @return integer 
     */
    public function getUserFlowRequirementId()
    {
        return $this->userFlowRequirementId;
    }

    /**
     * Set stepRequirementId
     *
     * @param integer $stepRequirementId
     * @return UserFlowRequirement
     */
    public function setStepRequirementId($stepRequirementId)
    {
        $this->stepRequirementId = $stepRequirementId;
    
        return $this;
    }

    /**
     * Get stepRequirementId
     *
     * @return integer 
     */
    public function getStepRequirementId()
    {
        return $this->stepRequirementId;
    }

    /**
     * Set organizationId
     *
     * @param integer $organizationId
     * @return UserFlowRequirement
     */
    public function setOrganizationId($organizationId)
    {
        $this->organizationId = $organizationId;
    
        return $this;
    }

    /**
     * Get organizationId
     *
     * @return integer 
     */
    public function getOrganizationId()
    {
        return $this->organizationId;
    }

    /**
     * Set stepId
     *
     * @param integer $stepId
     * @return UserFlowRequirement
     */
    public function setStepId($stepId)
    {
        $this->stepId = $stepId;
    
        return $this;
    }

    /**
     * Get stepId
     *
     * @return integer 
     */
    public function getStepId()
    {
        return $this->stepId;
    }

    /**
     * Set flowId
     *
     * @param integer $flowId
     * @return UserFlowRequirement
     */
    public function setFlowId($flowId)
    {
        $this->flowId = $flowId;
    
        return $this;
    }

    /**
     * Get flowId
     *
     * @return integer 
     */
    public function getFlowId()
    {
        return $this->flowId;
    }

    /**
     * Set statusRepositoryId
     *
     * @param integer $statusRepositoryId
     * @return UserFlowRequirement
     */
    public function setStatusRepositoryId($statusRepositoryId)
    {
        $this->statusRepositoryId = $statusRepositoryId;
    
        return $this;
    }

    /**
     * Get statusRepositoryId
     *
     * @return integer 
     */
    public function getStatusRepositoryId()
    {
        return $this->statusRepositoryId;
    }

    /**
     * Set accomplishedBy
     *
     * @param integer $accomplishedBy
     * @return UserFlowRequirement
     */
    public function setAccomplishedBy($accomplishedBy)
    {
        $this->accomplishedBy = $accomplishedBy;
    
        return $this;
    }

    /**
     * Get accomplishedBy
     *
     * @return integer 
     */
    public function getAccomplishedBy()
    {
        return $this->accomplishedBy;
    }

    /**
     * Set accomplisehdAt
     *
     * @param \DateTime $accomplisehdAt
     * @return UserFlowRequirement
     */
    public function setAccomplisehdAt($accomplisehdAt)
    {
        $this->accomplisehdAt = $accomplisehdAt;
    
        return $this;
    }

    /**
     * Get accomplisehdAt
     *
     * @return \DateTime 
     */
    public function getAccomplisehdAt()
    {
        return $this->accomplisehdAt;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     * @return UserFlowRequirement
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    
        return $this;
    }

    /**
     * Get filePath
     *
     * @return string 
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return UserFlowRequirement
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     * @return UserFlowRequirement
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    
        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
