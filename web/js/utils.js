var flatColor = function(h){
	var PHI = 0.618033988749895; 
	var s, v;
	if(h===undefined){
		hue = (Math.floor(Math.random()*(360 - 0 + 1)+0))/360;
		h = ( hue + ( hue / PHI )) % 360; 
	} 
	else h/=360;                                           
	v = Math.floor(Math.random() * (100 - 20 + 1) + 20);
	s = (v-10)/100;
	v = v/100;

	var r, g, b, i, f, p, q, t;
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
        r = Math.round(r * 255);
        g = Math.round(g * 255);
        b = Math.round(b * 255);

    var finalColor = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);

	this.h = h;
	this.s = s;
	this.v = v;
	this.r = r;
	this.g = g;
	this.b = b;
	this.hex = finalColor;

}

function viewBoard(id)
{
	$('.board-body').html('<h1 class="text-center"><i class="fa fa-spin fa-spinner"></i></h1>');
	var url1 = "{{path('backend_board_view',{'id':'_ID_'})}}";
	var url = url1.replace("_ID_",id);
	//console.log('url',url);
	$('.board-body').load(url,{},function(a,b)
	{
		if(b == 'error')
		{
			$('.board-body').html('<h1>Problema detectado</h1><p>Kaans ha detectado un problema al generar el calculo de los cuadros. Podría ser un problema temporal. Intente de nuevo más tarde y si el problema persiste contacte a soporte técnico</p>');
		}
	});
}

function generate_color(color_control)
{
    var x = new flatColor();

  	var this_color = x.r+x.g+x.b;

  	if($.inArray(this_color,color_control))
  	{	
	  	
	  	var one = x.r;
	  	var two = x.g;
	  	var tre = x.b;
	  	return 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
		//return 'rgb(' + one + ',' + two + ',' + tre +')';		  	
		
  	} else {
	  	generate_color(color_control);
	}
}