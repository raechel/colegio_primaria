<?php

	include "main.class.php";
	
	$main = new main();
	echo date("Y-m-d H:i:s")."\n";
	$clientsList = $main->getActiveUsersAndPlans();
	foreach($clientsList as $item)
	{
		echo "Revisando: ".$item['account_name']."\n";
		if(!$main->checkIfInvoiceAlreadyExists($item['client_agreement_id'],$item['service_id']))
		{
			$clientId       = $item['client_id'];
			$serviceId      = $item['service_id'];
			$organizationId = $item['organization_id'];
			$clientAgreementId = $item['client_agreement_id'];
			$amount         = $item['amount'];
			$discount       = $item['discount'];
			echo "-- Iniciando INSERT"."\n";
			if($main->insertInvoice($clientId,$serviceId,$organizationId,$clientAgreementId,$amount,$discount))
			{
				echo "--- ".$item['account_name']." con valor de ".$amount." y con descuento de ".$discount."\n";
			};			
		} else {
			echo "--- Ignorando porque ya existe factura emitida: ".$item['account_name']." - ".$item['client_agreement_id']."\n";
		}
	}
	
	


?>